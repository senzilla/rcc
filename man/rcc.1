.Dd $Mdocdate$
.Dt RCC 1 
.Sh NAME
.Nm rcc
.Nd RDFa content compiler
.Sh SYNOPSIS
.Nm
.Op Fl d Ar datatype 
.Op Fl t Ar template 
.Ar file 
.Sh DESCRIPTION
.Pp
The
.Nm rcc
command builds individual pieces of content\&. Input is
.Ar file
, formatted as RDFa Lite\&. Content is printed to stdout and is built
using a selection of customizable
.Xr mustache 5
templates\&.
.Pp
All standard templates use properties from the schema\&.org types
.Em Thing
,
.Em CreativeWork
and
.Em SoftwareApplication
\&. This
makes
.Nm rcc
a great tool for publishing personal websites, blogs and
software documentation in multiple formats\&. See
.Xr rdfaprimer 7
for
an introduction to RDFa Lite\&.
.Pp
The options are as follows:
.Bl -tag -width indent
.It Fl d Ar datatype 
Data type that string literals should be
formatted as. Valid arguments are
.It Fl t Ar template 
Name of a file in
.It Ar file  
Input file formatted as RDFa Lite.
.El
.Sh LOCALIZATION
.Pp

.Nm rcc
is able to manage content translated to multiple languages
if (i) your templates are translated and (ii) your content language is annotated
correctly in RDFa Lite properties\&.
.Pp
Templates are identified as
.Va name\&.language
with a dot separating
the template name and the intended language\&. A real example would be the
.Va blog\&.en
template\&.
.Pp
RDFa Lite content can make use of two important localization features (i)
annotating its own language with the
.Va lang
property and (ii) linking
between translations using the two properties below\&. See schema\&.org for more
information\&.
.Bl -tag -width indent
.It Va schema:workTranslation
Used on original work for linking to its translations\&.
.It Va schema:translationOfWork
Used on translations of original work for linking back\&.
.El
.Sh TEMPLATES
.Pp

.Nm rcc
is using
.Xr mustache 7
as its templating
language\&. When an RDFa Lite document is processed, each individual property
becomes available as a normal variable in any template its rendered with\&.
.Pp
Given the following simple RDFa Lite document:
.Bd -literal -offset indent
<article typeof="schema:webpage" resource="bonjour\&.html" lang="fr">
<h1 property="schema:headline">Bonjour monde</h1>
<p property="schema:abstract">Bienvenue sur mon site</p>
<p>Éteignez votre ordinateur!</p>
</article>
.Ed
.Pp
The following normal variables will be available:
.Bl -tag -width indent
.It Va {{schema\&.headline}}
With the value "Bonjour monde"\&.
.It Va {{schema\&.abstract}}
With the value "Bienvenue sur mon site"\&.
.El
.Pp
Additionally, three special variables will always be available:
.Bl -tag -width indent
.It Va {{schema\&.inLanguage}}
Derived from the
.Va lang
attribute with the value "fr"\&.
.It Va {{schema\&.webpage}}
Derived from the
.Va typeof
attribute with the value "1"\&.
.It Va {{_base}}
Populated with the value of the
.Ev BASE
environment variable\&.
.It Va {{_main}}
With the full HTML body as its value, even including HTML elements without
RDFa Lite attributes\&.
.El
.Pp
There are two kinds of templates (i) those meant to build single piece of
content (ii) those meant to build lists of content\&. List templates are used by
.Xr rcclist 1
come in pairs of two, for example
.Va blog
and
.Va blog\&.item
\&. The former is the wrapping template
and the latter is the template for each individual item in the list\&.
.Pp
Below is the list of standard templates\&. Unless specified, all templates will
render content with the schemas
.Em CreativeWork
or
.Em Thing
\&.
.Ss Single templates
.Bl -tag -width indent
.It html
Renders a basic HTML page\&.
.It man
Renders a Unix manual page formatted with
.Xr mdoc 7
for a
.Em SoftwareApplication
\&.
.It readme
Renders a README file formatted with
.Xr markdown 7
for a
.Em SoftwareApplication
\&.
.It xml
Renders the header for a XML page\&.
.El
.Ss List templates
.Bl -tag -width indent
.It blog
Renders an HTML list of blog posts\&.
.It feed
Renders an Atom XML syndication feed\&.
.It index
Renders an unordered HTML item list\&.
.It sitemap
Renders an XML sitemap feed\&.
.It terms
Renders a plain HTML list of terms and descriptions\&.
.El
.Sh BUILD SYSTEM
.Pp

.Nm rcc
comes with a simple content build system based on
.Xr make 1
\&. This system will build content automatically for a few
common scenarios, without the user having to interact with
.Nm rcc
at
all\&.
.Ss Website content
.Pp
For a simple website, a
.Va Makefile
like the sample below should be
placed at the document root of the website:
.Bd -literal -offset indent
DOMAIN= www\&.example\&.com
\&.include <rcc\&.html\&.mk>
\&.include <rcc\&.blog\&.mk>
\&.include <rcc\&.feed\&.mk>
.Ed
.Pp
The
.Va rcc\&.html\&.mk
file provide the base functionality with all
common
.Xr make 1
targets for building HTML pages\&. Content is treated
in two ways (i)
.Em posts
authored as plain
.Xr markdown 7

documents and (ii)
.Em pages
authored as RDFa Lite documents\&. Posts are any
file ending with
.Va *\&.md
\&. Pages are any file ending with
.Va *\&.rdfa
\&. In practice, the only difference between these two is that
posts go through an extra build step for processing the markdown to RDFa Lite\&.
From that point all files are equal\&.
.Sh ENVIRONMENT
.Pp
The following environment variables affect the execution:
.Bl -tag -width Ds
.It Ev BASE
Sets the value of in the template variable
.It Ev TEMPLATE
Affects which template that will be used.
Same as:
.El
.Sh EXAMPLES
.Pp
Given
.Bd -literal -offset indent
$ rcc -t html.en doc.rdfa
.Ed
.Pp
Given
.Bd -literal -offset indent
$ rcc -t software.en tool.rdfa
.Ed
.Pp
Create a pipeline converting
.Bd -literal -offset indent
$ lowdown post.md | triple-rdfa | rcc
.Ed
.Sh CAVEATS
.Pp
Due to a limitation in how
.Xr triple-turtle 1
currently parses
HTML, it\&'s required to have at least one non-void element before a
.Xr mustache 7
partial tag appear in the body\&.