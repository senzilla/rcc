# rcc – RDFa content compiler 

**rcc** is a semantic content compiler for RDFa Lite, written in `execline(1)`.
The various commands take structured RDFa Lite documents as input and are
able to produce different kinds of templated output such as HTML pages,
blog posts, XML feeds, readme files, man pages, etc.  

## Requirements

- [POSIX system](https://pubs.opengroup.org/onlinepubs/9699919799.2018edition)
- [`execline(1)`](https://skarnet.org/software/execline)
- [`sz-build(1)`](https://senzilla.io/software/sz-build)

## License

[Internet Software Consortium](https://opensource.org/licenses/ISC)

## Download

See https://git.senzilla.io/rcc 
