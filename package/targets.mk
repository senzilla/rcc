# SPDX-License-Identifier: ISC

BIN_TARGETS=	rcc rccif rcclist rccnew

SHARE_TARGETS=	blog.en.mustache blog.item.en.mustache feed.en.mustache \
		feed.item.en.mustache html.en.mustache index.en.mustache \
		index.item.en.mustache man.en.mustache none.en.mustache \
		readme.en.mustache sitemap.en.mustache \
		sitemap.item.en.mustache software.en.mustache \
		terms.en.mustache terms.item.en.mustache xml.en.mustache

SHARE_TARGETS+=	page.new post.new software.new

MAN_TARGETS=	contentstyle.7 rcc.1 rdfaprimer.7
