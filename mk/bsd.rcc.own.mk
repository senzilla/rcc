# SPDX-License-Identifier: ISC

.if exists(Makefile.inc)
.include "Makefile.inc"
.elif exists(../Makefile.inc)
.include "../Makefile.inc"
.endif

.if exists(${.CURDIR}/.policy)
.include "${.CURDIR}/.policy"
.elif exists(${.CURDIR}/../.policy)
.include "${.CURDIR}/../.policy"
.endif

.if exists(${.CURDIR}/package/targets.mk)
.include "${.CURDIR}/package/targets.mk"
.elif exists(${.CURDIR}/../package/targets.mk)
.include "${.CURDIR}/../package/targets.mk"
.endif

.if exists(${.CURDIR}/package/deps.mk)
.include "${.CURDIR}/package/deps.mk"
.elif exists(${.CURDIR}/../package/deps.mk)
.include "${.CURDIR}/../package/deps.mk"
.endif

LANG?=		en_US.UTF-8
XLANG!=		heredoc 0 ${LANG} cut -c -2 # Current language
PLANG?=		${XLANG} # Preferred language

DOMAIN?=	localhost
BASE?=		https://${DOMAIN}
ENV?=		env BASE=${BASE}

TIDY?=		tidy -errors -quiet --show-filename yes --show-info no \
		--show-body-only yes --drop-empty-elements no

RROOT!=		pipeline find . -name "*.rdfa" -maxdepth 1 '' cut -c 3-
RDIRS!=		pipeline find . -type d -maxdepth 1 -not -path "*/.*" -exec test ! -e {}/Makefile \; -print '' cut -c 3-

.if !empty(RDIRS)
MPOSTS!=	pipeline find ${RDIRS} -name "*.md" '' sort -r
RPOSTS?=	${MPOSTS:.md=.rdfa}
RPAGES!=	find ${RDIRS} -name "*.rdfa"
.endif

RLISTS!=	pipeline find . -name "*.rcclist" '' cut -c 3-
MLISTS?=	${RLISTS:.rcclist=.mustache}

# Web targets
WROOT?=		${RROOT:.rdfa=.html}
WPAGES?=	${RPAGES:.rdfa=.html}
WPOSTS?=	${RPOSTS:.rdfa=.html}
WTARGETS?=	${RPOSTS} ${WPOSTS} ${WROOT} ${WPAGES} ${MLISTS}
WINSTALL!=	pipeline find . -name "*.html" -o -name "*.xml" -o \
		-name "*.jpg" -o -name "*.png" -o -name "*.css" '' cut -c 3-

# Installation
INSTALL=	sz-install
DESTDIR?=	/var/www/htdocs/${DOMAIN}

RCC_OWN_MK=Done
