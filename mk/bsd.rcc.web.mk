# SPDX-License-Identifier: ISC

.include <rcc.own.mk>

.MAIN: all
.PHONY: all clean test test-rdfa test-html
.SUFFIXES: .html .md .mustache .rcclist .rdfa

all: ${WTARGETS}

clean:
.for f in ${WTARGETS}
	rm -f ${f}
.endfor

.md.rdfa:
	${ENV} redirfd -w 1 $@ triple $<

.rdfa.html:
	${ENV} cd ${<D} redirfd -w 1 ${@F} rcc ${<F}

.rcclist.mustache:
	${ENV} cd ${<D} ./${<F} redirfd -w 1 ${@F} rcclist

.for f in ${WINSTALL}
${DESTDIR}/${f}: ${f}
	${INSTALL} -Dm 0444 $> $@
.endfor

test: test-rdfa test-html

test-rdfa: ${RPOSTS} ${RPAGES}
	@${TIDY} $>

test-html: ${TPOSTS} ${TPAGES}
	@${TIDY} $>
