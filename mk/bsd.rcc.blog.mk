# SPDX-License-Identifier: ISC

blog/index.${XLANG}.html: ${RPOSTS}
	${ENV} pipeline echo "$>" '' pipeline tr " " "\n" '' redirfd -w 1 $@ ecclist -t blog.${XLANG}.html index.${XLANG}.rdfa

TARGETS+=	blog/index.${XLANG}.html
