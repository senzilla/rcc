# SPDX-License-Identifier: ISC

.include <rcc.own.mk>
.include <bsd.subdir.mk>

MAKE=	${ENV} make
