# SPDX-License-Identifier: ISC

.include <rcc.own.mk>

feed.${XLANG}.xml sitemap.${XLANG}.xml:
	${ENV} pipeline echo "$>" '' pipeline tr " " "\n" '' redirfd -w 1 $@ rcclist -t ${@F} index.${XLANG}.rdfa

sitemap.xml: sitemap.${XLANG}.xml
	ln -s $> $@

feed.${XLANG}.xml: ${RPOSTS}
sitemap.${XLANG}.xml: ${RPOSTS} ${RPAGES}

WTARGETS+=	feed.${XLANG}.xml sitemap.${XLANG}.xml sitemap.xml
