# SPDX-License-Identifier: ISC

.include <rcc.own.mk>

.MAIN: all
.PHONY: all install

ROOT?=	..
MANDIR=	${ROOT}/man

.for m in ${MAN_TARGETS}
_MAN_TARGET=${MANDIR}/${m:T}
all: ${_MAN_TARGET}
test: ${m}.rdfa

${_MAN_TARGET}: ${m}.rdfa
	${ENV} redirfd -w 1 $@ rcc -dman -tman.${PLANG} $>
.endfor

.if defined(README)
all: ${ROOT}/README.md
test: ${README}

${ROOT}/README.md: ${README}
	${ENV} redirfd -w 1 $@ rcc -dmd -treadme.${PLANG} $>
.endif

test:
	@${TIDY} $>
