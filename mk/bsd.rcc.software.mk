# SPDX-License-Identifier: ISC

.include <rcc.own.mk>

SPAGES=	${INDEX} ${PAGES}
RPAGES=	${PAGES:%=${DIR}/%}
TPAGES=	${SPAGES:.rdfa=.${XLANG}.html}

.MAIN: all
_INDEX=${INDEX:%.rdfa=%.${XLANG}.rdfa}
all: ${_INDEX}
install: ;

${_INDEX}: ${INDEX:%=${DIR}/%}
	cp $> $@

clean:
	@rm -f ${TPAGES}

.for s in ${SPAGES}
_PAGE=${s:%.rdfa=%.${XLANG}.html}
all: ${_PAGE}

${_PAGE}: ${DIR}/${s:T}
	${ENV} rcc -dhtml -tsoftware.${XLANG} ${DIR}/${s:T} > $@
.endfor

test: ${RPAGES}
	@${TIDY} $>
